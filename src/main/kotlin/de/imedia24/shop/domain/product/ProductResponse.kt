package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime

data class ProductResponse(
    val sku: String,
    val name: String,
    val description: String,
    val price: BigDecimal
) {
    companion object {
        fun ProductEntity.toProductResponse() = ProductResponse(
            sku = sku,
            name = name,
            description = description ?: "",
            price = price
        )
    }
    fun toProductEntity(): ProductEntity {
        return ProductEntity(
            sku = sku,
            name = name,
            description = description,
            price = price,
            createdAt = ZonedDateTime.now(),
            updatedAt = ZonedDateTime.now()
        )
    }
}
