package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import javassist.NotFoundException
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse {
        val productEntity = productRepository.findBySku(sku)
        return productEntity?.toProductResponse() ?: throw NotFoundException("Product not found")
    }

    fun addProduct(product: ProductEntity): ProductEntity {
        return productRepository.save(product)
    }

    fun findAllProducts(): List<ProductEntity> {
        return productRepository.findAll().toList()
    }

    fun findProductsBySkus(skus: List<String>): List<ProductEntity> {
        return productRepository.findProductsBySkus(skus)
    }

    fun updateProduct(sku: String, updateDTO: ProductResponse): ProductEntity {
        val existingProduct = productRepository.findById(sku)
            .orElseThrow { NotFoundException("Product not found with SKU: $sku") }

        updateDTO.name?.let { existingProduct.name = it }
        updateDTO.description?.let { existingProduct.description = it }
        updateDTO.price?.let { existingProduct.price = it }

        return productRepository.save(existingProduct)
    }
}
