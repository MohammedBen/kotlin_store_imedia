package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun getProductsBySkus(@RequestParam("skus") skus: List<String>): List<ProductEntity> {

        logger.info("Request for product $skus")
        return productService.findProductsBySkus(skus)
    }

    @GetMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
            logger.info("Request for product $sku")

            val product = productService.findProductBySku(sku)
            return ResponseEntity.ok(product)

    }

    @PostMapping("/product")
    fun addProduct(@RequestBody product: ProductResponse): ProductEntity {
        return productService.addProduct(product.toProductEntity())
    }

    @GetMapping("/all")
    fun getAllProducts(): List<ProductEntity> {
        return productService.findAllProducts()
    }
    @PatchMapping("/products/{sku}")
    fun partiallyUpdateProduct(
        @PathVariable("sku") sku: String,
        @RequestBody updateDTO: ProductResponse
    ): ResponseEntity<ProductEntity> {
        val updatedProduct = productService.updateProduct(sku, updateDTO)
        return ResponseEntity.ok(updatedProduct)
    }
}
